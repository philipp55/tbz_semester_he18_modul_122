﻿# Bitte im Ordner "Music" einen Ordner "Default" erstellen
# Musik Pfad / Bitte ändern mit eigenem
$MusikPfad = "C:\Users\Phili\Music\Default"
# Bitte das Bild welches im BSCW ist, an einen Ort speichern und dann hier den Link einfügen
$BildPfad = "$PSScriptRoot\Background.jpg"

# Suchfunktion für den Lied-Download
$Suchen = {
    $keyCode = ([int]$_.KeyChar)
    if ($keyCode -eq 13) {

        Write-Host $SearchBar.Text
        Lied-Download $SearchBar.Text #Wenn man "Enter" drückt wird dieser Text in die Variabel $SearchBar gespeichert
        
    }
}

# Herunterladen eines Songs
function Lied-Download
{
Begin
{
$query='https://www.youtube.com/results?search_query='
}
Process
{
if ($args.Count -eq 0)
{
"Args were empty, commiting `$input to `$args"
Set-Variable -Name args -Value (@($input) | % {$_})
"Args now equals $args"
$args = $args.Split()
}
ELSE
{
"Args had value, using them instead"
}

Write-Host $args.Count, "Arguments detected"
"Parsing out Arguments: $args"
for ($i=0;$i -le $args.Count;$i++){
$args | % {"Arg $i `t $_ `t Length `t" + $_.Length, " characters"} }

$args | % {$query = $query + "$_+"}

}
End
{
$url = $query.Substring(0,$query.Length-1)
"Final Search will be $url `nInvoking..."
$lied = Invoke-WebRequest -Uri "$url" 

$content = $lied.Content
$IndexOf = $content.IndexOf("data-context-item-id")
$IndexOf = $IndexOf + 22
$Substring = $content.Substring($IndexOf, 11)
Write-Host $Substring
$download = 'https://www.1010diy.com/mp3?url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D'+$substring+'%26quality=128k'
start $download
}
}

# ALLE FENSTER

# Fenster 1 (Home Screen)
$MainFenster = New-Object Windows.Forms.Form #Fenster erstellen
$MainFenster.Size = New-Object System.Drawing.Size(400,600) # ändern auf "400,600"
$MainFenster.StartPosition = "CenterScreen" #Fenster öffnet sich in der mitte des PC's
$MainFenster.Text = "Spotimy" #Titel des Fensters

# Fenster 2 (für den Ordner Namen)
$NeuerOrdner = New-Object Windows.Forms.Form #Fenster erstellen
$NeuerOrdner.Size = New-Object System.Drawing.Size(400,201) # Fenstergrösse
$NeuerOrdner.StartPosition = "CenterScreen" #Fenster öffnet sich in der mitte des PC's
$NeuerOrdner.Text = "Neuen Ordner erstellen" #Titel des Fensters
$NeuerOrdner.BackColor = "Black"

# ALLE DINGE FÜR DIE HAUPTSEITE

# Musik Suchleiste
$SearchBar = New-Object System.Windows.Forms.TextBox #Textbox erstellen
$SearchBar.Add_KeyPress($Suchen)
$SearchBar.Left = 125
$SearchBar.Width = 150
$SearchBar.Height = 100
$SearchBar.Top = 240
$SearchBar.BackColor = '#083379' #Hintergrundfarbe Gelb

#Button für's Musik abspielen
$MusikSpielen = New-Object System.Windows.Forms.Button
$MusikSpielen.Text = "Play"
$MusikSpielen.Left = 150
$MusikSpielen.Width = 100
$MusikSpielen.Height = 75
$MusikSpielen.Top = 470
$MusikSpielen.Add_Click($musik)
$MusikSpielen.BackColor = '#146BF6'

#Button für's Musik stoppen
$MusikStoppen = New-Object System.Windows.Forms.Button
$MusikStoppen.Text = "II"
$MusikStoppen.Left = 150
$MusikStoppen.Width = 100
$MusikStoppen.Height = 75
$MusikStoppen.Top = 470
$MusikStoppen.Add_Click($MusikStopp)
$MusikStoppen.BackColor = '#146BF6'
$FontColor2 = New-Object System.Drawing.Font("Arial", 16) # Grösse und Schriftart des Buttons
$MusikStoppen.Font = $FontColor2

#Button für's Musik wieder abspielen
$MusikPlayNochmals = New-Object System.Windows.Forms.Button
$MusikPlayNochmals.Text = "▶"
$MusikPlayNochmals.Left = 150
$MusikPlayNochmals.Width = 100
$MusikPlayNochmals.Height = 75
$MusikPlayNochmals.Top = 470
$MusikPlayNochmals.Add_Click($MusikPlayWieder)
$MusikPlayNochmals.BackColor = '#146BF6'
$FontColor3 = New-Object System.Drawing.Font("Arial", 16) # Grösse und Schriftart des Buttons
$MusikPlayNochmals.Font = $FontColor3

# "Commandlet" für's Musik abspielen
$musik = {
   Add-Type -AssemblyName presentationCore
  
$global:mediaPlayer = New-Object System.Windows.Media.MediaPlayer
$global:mediaPlayer.open([uri]"$($file.fullname)")
 
$musicFiles = Get-ChildItem -path $MusikPfad -include *.mp3,*.wma -recurse
 
foreach($file in $musicFiles)
{
 "Playing $($file.BaseName)"
$global:mediaPlayer.open([uri]"$($file.fullname)")
$duration = $global:mediaPlayer.NaturalDuration.TimeSpan.TotalSeconds
$global:mediaPlayer.Play()
$musikspielen.Dispose()
}}

# "Commandlet" für's Musik stoppen
$MusikStopp = {
$Global:mediaplayer.Pause()
$MusikStoppen.Hide()
$MusikPlayNochmals.Show()
}

# "Commandlet" für's Musik wieder abspielen
$MusikPlayWieder = {
$Global:mediaplayer.Play()
$MusikPlayNochmals.Hide()
$MusikStoppen.Show()
}

# Button zum öffnen der "Playlist erstellen" Seite
$Playlist1 = New-Object System.Windows.Forms.Button #Button erstellen
$Playlist1.Text = "Neue Playlist"
$Playlist1.Left = 150
$Playlist1.Width = 100
$Playlist1.Height = 75
$Playlist1.Top = 150
$Playlist1.Add_Click({$NeuerOrdner.ShowDialog()}) #Playlist Fenster erstellen öffnen
$Playlist1.BackColor = '#083379'

# Textfeld um den Namen einzugeben
$SearchBarOrdner = New-Object System.Windows.Forms.TextBox #Textbox erstellen
$SearchBarOrdner.Add_KeyPress($OrdnerErstellenCMD)
$SearchBarOrdner.Left = 120
$SearchBarOrdner.Width = 150
$SearchBarOrdner.Height = 100
$SearchBarOrdner.Top = 100
$SearchBarOrdner.BackColor = 'Yellow' #Hintergrundfarbe Gelb

# Label fürs Ordner erstellen
$OrdnerLabel = New-Object System.Windows.Forms.Label #Label erstellen
$OrdnerLabel.Text = "Wie soll die Playlist heissen?" #Titel des Fensters
$OrdnerLabel.ForeColor = 'White' #Hintergrundfarbe Weiss
$OrdnerLabel.Width = 350
$OrdnerLabel.Height = 100
$OrdnerLabel.Left = 20
$OrdnerLabel.TextAlign = "MiddleCenter"
$FontColor1 = New-Object System.Drawing.Font("Arial", 12) # Grösse und Schriftart des Labels
$OrdnerLabel.Font = $FontColor1

# Ordner Erstellung
 $OrdnerErstellenCMD = {
    $keyCode = ([int]$_.KeyChar)
    if ($keyCode -eq 13) {
        Write-Host $SearchBarOrdner.Text
        $OrdnerName = $SearchBarOrdner.Text
        New-Item -Path ".\Music\" -Name "$OrdnerName" -ItemType directory
    }}

# Hintergrundbild
$BackgroundPicture = New-Object System.Windows.Forms.PictureBox
$BackgroundPicture.Image = [System.Drawing.Image]::FromFile($BildPfad)
$BackgroundPicture.ClientSize = New-Object System.Drawing.Size (1000,1000)
$BackgroundPicture.SizeMode = 1

# EINZELTEILE HINZUFÜGEN

# Alle Einzelteile zum Programm hinzufügen
$MainFenster.Controls.Add($SearchBar)
$MainFenster.Controls.Add($Musikspielen)
$MainFenster.Controls.Add($Playlist1)
$NeuerOrdner.Controls.Add($SearchBarOrdner)
$NeuerOrdner.Controls.Add($OrdnerLabel)
$MainFenster.Controls.Add($MusikStoppen)
$MainFenster.Controls.Add($MusikPlayNochmals)
$MainFenster.Controls.Add($BackgroundPicture)

$MusikPlayNochmals.Hide()

#Formular starten und anzeigen
$MainFenster.ShowDialog() | Out-Null

# Do / While Schleife für die Programm schliessen Abfrage
do{

$Confirm = New-Object Windows.Forms.Form #Fenster erstellen
$Confirm.Size = New-Object System.Drawing.Size(500,201) # Fenstergrösse
$Confirm.StartPosition = "CenterScreen" #Fenster öffnet sich in der mitte des PC's
$Confirm.Text = "Spotimy beenden?" #Titel des Fensters
$Confirm.BackColor = 'Black' #Hintergrundfarbe Schwarz

$ConfirmLabel = New-Object System.Windows.Forms.Label #Label erstellen
$ConfirmLabel.Text = "Wollen Sie das Programm wirklich beenden?" #Titel des Fensters
$ConfirmLabel.ForeColor = 'White' #Hintergrundfarbe Weiss
$ConfirmLabel.Width = 350
$ConfirmLabel.Height = 100
$ConfirmLabel.Left = 75
$ConfirmLabel.TextAlign = "MiddleCenter"
$FontColor = New-Object System.Drawing.Font("Arial", 14) # Grösse und Schriftart des Labels
$ConfirmLabel.Font = $FontColor

$ButtonJA = New-Object System.Windows.Forms.Button
$ButtonJA.Text = "Ja"
$ButtonJA.Left = 0
$ButtonJA.Width = 250
$ButtonJA.Height = 56
$ButtonJA.Top = 90
$ButtonJA.Add_Click({
    $Confirm.dispose()
    # Prüfen, ob der MediaPlayer läuft
    if ($Global:mediaplayer.NaturalDuration.HasTimeSpan -and $Global:mediaplayer.Position -lt $Global:mediaplayer.NaturalDuration.TimeSpan) {
    # MediaPlayer stoppen, wenn er läuft
    $Global:mediaplayer.Stop()
    }

    })
$ButtonJA.BackColor = '#424242'
$ButtonJA.Font = $FontColor
$ButtonJA.ForeColor = 'White'

$ButtonNEIN = New-Object System.Windows.Forms.Button
$ButtonNEIN.Text = "Nein"
$ButtonNEIN.Left = 245
$ButtonNEIN.Width = 235
$ButtonNEIN.Height = 56
$ButtonNEIN.Top = 90
$ButtonNEIN.Add_Click({
    $Confirm.Dispose()
    $MainFenster.ShowDialog()
    })
$ButtonNEIN.BackColor = '#424242'
$ButtonNEIN.Font = $FontColor
$ButtonNEIN.ForeColor = 'White'
$buttonJA.DialogResult = "OK"

$Confirm.Controls.Add($ButtonJA)
$Confirm.Controls.Add($ButtonNEIN)
$Confirm.Controls.Add($ConfirmLabel)
$CloseWindowResult = $Confirm.ShowDialog()


}while ($CloseWindowResult -ne "OK")

# Damit die Musik auch sicher aufhört, wenn man das Programm schliesst
# Prüfen, ob der MediaPlayer läuft
if ($Global:mediaplayer.NaturalDuration.HasTimeSpan -and $Global:mediaplayer.Position -lt $Global:mediaplayer.NaturalDuration.TimeSpan) {
    # MediaPlayer stoppen, wenn er läuft
    $Global:mediaplayer.Stop()
}

# Ausgabe, wenn das Programm geschlossen wird
Write-Host "Wir wünschen dir einen schönen Tag." #Nachricht wenn man das Programm schliesst
Write-Host "Wir freuen uns, wenn du wieder kommst." #Nachricht wenn man das Programm schliesst